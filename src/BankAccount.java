package src;

import java.util.GregorianCalendar;

public class BankAccount {
    private int id;
    private double balance = 0.00;
    private static double annualInterestRate =1; //Global between Bank accounts
    private GregorianCalendar dataCreated = new GregorianCalendar();
    public BankAccount(){
    }
    public BankAccount(int id, double balance){
        this.id = id;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getAnnualInterestRate() {
        return annualInterestRate;
    }
    public void setAnnualInterestRate(double annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
    }

    public GregorianCalendar getDataCreated() {
        return dataCreated;
    }

    public double getMonthlyInterestRate(){
        return annualInterestRate/12;
    }
    public void withdraw(double amount){
        balance -= amount;
        System.out.println(amount + " has been withdrawn from:\t" + getId());
    }
    public void deposit(double amount){
        balance += amount;
        System.out.println(amount + " has been deposited into:\t" + getId());
    }
    public String toString(){
        String leString = new String("Bank Registration Number: " + this.getId() + ":\n\t Balance:\t" + this.getBalance());
        return leString;
    }
}
