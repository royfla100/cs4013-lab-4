package src;

public class Person {
    private String name, address, phoneNo, email;
    public Person(){

    }
    public Person(String name, String address, String phoneNo, String email){
        this.name = name;
        this.address = address;
        this.phoneNo = phoneNo;
        this.email = email;
    }

    public String getName() {
        return name;
    }
    public String getAddress() {
        return address;
    }
    public String getPhoneNo() {
        return phoneNo;
    }
    public String getEmail() {
        return email;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String toString(){
        return "\nName:\t" + name + "\tAddress:\t" + address + "\tPhone No.:\t" + phoneNo + "\tE-Mail:\t" + email + "\n";
    }
}
