package src;

public class Student extends Person{
    private int yearOfStudy = 0;
    private String programmeOfStudy;
    public Student(){

    }
    public Student(int yearOfStudy, String programmeOfStudy){
        this.yearOfStudy = yearOfStudy;
        this.programmeOfStudy = programmeOfStudy;
    }
    public Student(String name, String address, String phoneNo, String email){
        super(name,address,email,phoneNo);
    }
    public Student(String name, String address, String phoneNo, String email, int yearOfStudy, String programmeOfStudy){
        super(name,address,email,phoneNo);
        this.yearOfStudy = yearOfStudy;
        this.programmeOfStudy = programmeOfStudy;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public void setProgrammeOfStudy(String programmeOfStudy) {
        this.programmeOfStudy = programmeOfStudy;
    }

    public String getProgrammeOfStudy() {
        return programmeOfStudy;
    }

    @Override
    public String toString() {
        return super.toString() + "\tStatus:\tStudent\tYear of Study:\t" + yearOfStudy + "\tProgramme of Study:\t" + programmeOfStudy + "\n";
    }
}
