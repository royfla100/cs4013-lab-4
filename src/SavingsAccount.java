package src;

public class SavingsAccount extends BankAccount{
    public SavingsAccount(){
    }
    public SavingsAccount(int id, double balance){
        super(id, balance);
    }
    @Override
    public void withdraw(double amount){
        double temp = super.getBalance();
        if (amount <= temp){
            temp -= amount;
            super.setBalance(temp);
        } else System.out.println(super.getId() + " does not have enough balance to withdraw this amount.");
    }

    @Override
    public String toString() {
        return super.toString() + "\tType:\tSavings.";
    }
}
