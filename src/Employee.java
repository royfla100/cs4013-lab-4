package src;

public class Employee extends Person{
    private String office = "No Office";
    private int annualSalary = 0;
    public Employee(){

    }
    public Employee(String name, String address, String phoneNo, String email){
        super(name,address,email,phoneNo);
    }
    public Employee(String name, String address, String phoneNo, String email, String office, int annualSalary){
        super(name,address,email,phoneNo);
        this.office = office;
        this.annualSalary = annualSalary;
    }

    public int getAnnualSalary() {
        return annualSalary;
    }
    public String getOffice(){
        return office;
    }

    public void setAnnualSalary(int annualSalary) {
        this.annualSalary = annualSalary;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    @Override
    public String toString() {
        return super.toString() + "\tStatus:\tEmployee\tOffice:\t" + office + "\tAnnual Salary:\t" + annualSalary;
    }
}
