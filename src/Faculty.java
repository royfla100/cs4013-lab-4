package src;

public class Faculty extends Employee{
    private String officeHours;
    private String rank;
    public Faculty(){

    }
    public Faculty(String name, String address, String phoneNo, String email, String office, int annualSalary){
        super(name, address, phoneNo, email, office, annualSalary);
    }
    public Faculty(String name, String address, String phoneNo, String email, String office, int annualSalary, String officeHours, String rank){
        super(name, address, phoneNo, email, office, annualSalary);
        this.officeHours = officeHours;
        this.rank = rank;
    }

    public String getRank() {
        return rank;
    }

    public void setOfficeHours(String officeHours) {
        this.officeHours = officeHours;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getOfficeHours() {
        return officeHours;
    }
    public String toString(){
        return super.toString() + "\tHours:\t" + officeHours + "\tRank:\t" + rank;
    }
}
