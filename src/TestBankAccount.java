package src;

import java.util.ArrayList;

public class TestBankAccount {

    public static void main(String[] args){

        double interestRate = 5;
        CurrentAccount Roy = new CurrentAccount(21302545, 9000.0);
        SavingsAccount Yor = new SavingsAccount(19456341,20);
        BankAccount Joe = new BankAccount(19132524,999);
        ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();

        accounts.add(Roy);
        accounts.add(Yor);
        accounts.add(Joe);
        printAccounts(accounts);



        accounts.get(0).withdraw(9100);
        accounts.get(1).withdraw(1000);
        accounts.get(2).deposit(1000);
        printAccounts(accounts);
        accounts.get(0).withdraw(401);
        accounts.get(1).deposit(1000);
        accounts.get(2).withdraw(900);
        printAccounts(accounts);
        accounts.get(0).deposit(19000);
        Roy.setAnnualInterestRate(interestRate);
        printAccounts(accounts);

    }

    private static void printAccounts(ArrayList<BankAccount> accounts) {
        System.out.println("\n\t\t\t_____BANK ACCOUNTS:_____\nAnnual Interest Rate:\t" + accounts.get(0).getAnnualInterestRate() + "%");
        for (int i = 0; i < accounts.size(); i++){
            System.out.println(accounts.get(i).toString());
            System.out.println("Monthly interest rate:\t" + accounts.get(i).getMonthlyInterestRate());
        }
    }
}
