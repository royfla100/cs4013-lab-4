package src;

public class Staff extends Employee{
    private String title;
    public Staff(String name, String address, String phoneNo, String email, String office, int annualSalary){
        super(name, address, phoneNo, email, office, annualSalary);
    }
    public Staff(String name, String address, String phoneNo, String email, String office, int annualSalary, String title){
        super(name, address, phoneNo, email, office, annualSalary);
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return super.toString() + "\tTitle:\t"+title;
    }
}
