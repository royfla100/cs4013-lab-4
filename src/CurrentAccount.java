package src;

public class CurrentAccount extends BankAccount{
    private double overdraftLimit = 500;
    public CurrentAccount()
    {
    }
    public CurrentAccount(int id, double balance)
    {
        super(id, balance);
    }

    @Override
    public void withdraw(double amount) {
        if (amount <= (super.getBalance() + overdraftLimit))
        {
        super.withdraw(amount);
    } else {
        System.out.println(super.getId() + " has exceeded their overdraft limit.\tNo funds will be withdrawn.");
    }}

    @Override
    public String toString(){
        return (super.toString() + "\t Overdraft Limit:\t" + overdraftLimit + "\tType:\tCurrent.");
        }
}
